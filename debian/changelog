sus (7.20161013) unstable; urgency=medium

  * New upstream release: contains SUSv4 TC2; update checksum
    (Closes: #840318)
  * urgency=medium since susv4 is no longer installable
  * debian/compat: Use debheloper v10
  * debian/control:
    - Bump Standards-Version to 3.9.8 (No changes needed)

 -- David Weinehall <tao@debian.org>  Thu, 13 Oct 2016 14:15:32 +0300

sus (7.20160312) unstable; urgency=medium

  * The upstream tarball for SUSv4 TC1 changed; update checksum
    (Closes: #817819)
  * urgency=medium since susv4 is no longer installable
  * debian/control:
    - Bump Standards-Version to 3.9.7 (No changes needed)

 -- David Weinehall <tao@debian.org>  Sat, 12 Mar 2016 03:27:19 +0200

sus (7.20160107) unstable; urgency=medium

  * The upstream tarball for SUSv4 TC1 changed; update checksum
    (Closes: #790535)
    | The chapters on m4 and expr seems to have been improved slightly
  * urgency=medium since susv4 is no longer installable

 -- David Weinehall <tao@debian.org>  Thu, 07 Jan 2016 12:24:24 +0200

sus (7.20150719) unstable; urgency=medium

  * The upstream tarball for SUSv4 TC1 has changed; update checksum
    (Closes: #790535)
    | No normative changes, only tidying
  * urgency=medium since susv4 is no longer installable

 -- David Weinehall <tao@debian.org>  Sun, 19 Jul 2015 17:40:24 +0300

sus (7.20140918) unstable; urgency=low

  * Re-run install-docs during postinst after downloading & installing
    the documentation; when install-docs is run during initial install
    the tarballs have not yet been downloaded, thus the doc-base
    update fails
    (Closes: #733451)
    | Note that the warning from install-docs about the Format-section
    | being invalid when first installing these packages will still
    | be issued.
    | I do not know of a good way to solve this; suggestions welcome!
  * Added Lintian overrides to silence the warnings about
    useless calls to install-docs
  * debian/control:
    - Bump Standards-Version to 3.9.6 (No changes needed)

 -- David Weinehall <tao@debian.org>  Thu, 18 Sep 2014 01:40:02 +0300

sus (7.20140813) unstable; urgency=medium

  * The upstream tarball for SUSv4 TC1 has changed; update checksum
    | No normative changes, only tidying
  * urgency=medium since susv4 is no longer installable

 -- David Weinehall <tao@debian.org>  Wed, 13 Aug 2014 21:39:50 +0300

sus (7) unstable; urgency=low

  * Initial release of SUSv4
    (Closes: #644292)
  * Checksum downloaded files before unpacking them,
    to ensure that we get what we're expecting
    (Closes: #545238, #545239)
  * Added overrides for Lintian warning about doc-base referencing
    missing files
    | Since this is an installer package the files are not in place
    | when Lintian is run

 -- David Weinehall <tao@debian.org>  Wed, 05 Oct 2011 16:24:20 +0300
